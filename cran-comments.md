## Test environments
- i386-w64-mingw32/i386 (32-bit); Windows 7 x64; R version 3.3.3 (package development) and R devel version (winbuilder)
- i686-pc-linux-gnu (32-bit); Ubuntu 17.10; R version 3.4.2
- x86_64-apple-darwin13.4.0 (64-bit); OS X 10.11.6; R version 3.2.3


## R CMD check results
0 errors, 0 warnings, 0 notes


## Downstream dependencies
there are currently no downstream dependencies for this package.